package graphQL

import (
	
	A	"git.duniter.org/gerard94/util/avl"
	J	"git.duniter.org/gerard94/util/json"

)

func makeErrors (mk *J.Maker, errors *A.Tree) {
	if errors != nil && !errors.IsEmpty() {
		mk.StartArray()
		for e := errors.Next(nil); e != nil; e = errors.Next(e) {
			el := e.Val().(*ErrorElem)
			mk.StartObject()
			mk.PushString(el.Message)
			mk.BuildField("message")
			if el.Locations != nil {
				mk.StartArray()
				for _, loc := range el.Locations {
					mk.StartObject()
					mk.PushInteger(int64(loc.P))
					mk.BuildField("position")
					mk.PushInteger(int64(loc.L))
					mk.BuildField("line")
					mk.PushInteger(int64(loc.C))
					mk.BuildField("column")
					mk.BuildObject()
				}
				mk.BuildArray()
				mk.BuildField("locations")
			}
			p := el.Path
			if p != nil {
				mk.StartArray()
				for {
					switch p := p.(type) {
					case *PathString:
						mk.PushString(p.S)
					case *PathNb:
						mk.PushInteger(int64(p.N))
					}
					p = p.Next()
					if p == nil {break}
				}
				mk.BuildArray()
				mk.BuildField("path")
			}
			mk.BuildObject()
		}
		mk.BuildArray()
		mk.BuildField("errors")
	}
} //makeErrors

func makeValue (mk *J.Maker, value Value) {
	
	makeList := func (lv *ListValue) {
		mk.StartArray()
		for l := lv.First(); l != nil; l = lv.Next(l) {
			makeValue(mk, l.Value)
		}
		mk.BuildArray()
	} //makeList
	
	//makeValue
	switch value := value.(type) {
	case *IntValue:
		mk.PushInteger(value.Int)
	case *FloatValue:
		mk.PushFloat(value.Float)
	case *StringValue:
		if value == nil {
			mk.PushNull()
		} else {
			mk.PushString(value.String.S)
		}
	case *BooleanValue:
		mk.PushBoolean(value.Boolean)
	case *NullValue:
		mk.PushNull()
	case *EnumValue:
		mk.PushString(value.Enum.S)
	case *ListValue:
		makeList(value)
	case *OutputObjectValue:
		makeObject(mk, value)
	}
} //makeValue

func makeObject (mk *J.Maker, ov *OutputObjectValue) {
	
	makeField := func (f *ObjectField) {
		makeValue(mk, f.Value)
		mk.BuildField(f.Name.S)
	} //makeField
	
	//makeObject
	mk.StartObject()
	for f := ov.First(); f != nil; f = ov.Next(f) {
		makeField(f)
	}
	mk.BuildObject()
} //makeObject

func buildInstant (mk *J.Maker, data *OutputObjectValue) {
	if data != nil {
		makeObject(mk, data)
		mk.BuildField("data")
	}
} //buildInstant

func ResponseToJson (r Response) J.Json {
	mk := J.NewMaker()
	mk.StartObject()
	makeErrors(mk, r.Errors())
	switch r := r.(type) {
	case *InstantResponse:
		buildInstant(mk, r.Data)
	case *SubscribeResponse:
	}
	mk.BuildObject()
	return mk.GetJson()
} //ResponseToJson
