{
	arguments {
		... missingRequiredArg
	}
}

fragment missingRequiredArg on Arguments {
  nonNullBooleanArgField(nonNullBooleanArg: null)
}
