{
	dog {
		... conflictingBecauseAlias
	}
}

fragment conflictingBecauseAlias on Dog {
  name: nickname
  name
}
