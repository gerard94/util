package alea

type (
	
	Generator struct {
	}

)

// Generates a new pseudo-random real number x such that  (0 <= x < 1), with uniforme deviates.
func (g *Generator) Random() float64

// Generates a new integer number between min (included) and max (excluded)
func (g *Generator) IntRand(min, max int64) int64

// Generates a new unsigned integer number between min (included) and max (excluded)
func (g *Generator) UintRand(min, max uint64) uint64

// Generates a new pseudo-random real number x with gaussian (normal) deviates. Probability distribution p(x) = 1 / sqrt(2 * pi) * exp(-x^2 / 2)
func (g *Generator) GaussRand() float64

// Initializes the pseudo-random number generator with seed.
func (g *Generator) Randomize(seed int64)

// Initializes the generator with a random seed.
func New() *Generator

// Same as Generator.Random () with the default generator.
func Random() float64

// Same as Generator.IntRand () with the default generator.
func IntRand(min, max int64) int64

// Same as Generator.UintRand () with the default generator.
func UintRand(min, max uint64) uint64

// Same as Generator.GaussRand () with the default generator.
func GaussRand() float64

// Same as Generator.Randomize () with the default generator.
func Randomize(seed int64)
